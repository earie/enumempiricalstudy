#include <stdio.h>

 enum DAYS {SUNDAY = 1 , MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY,
 SATURDAY} day;

int main() {
  int d;
  printf("Enter day: \n");
  scanf("%d", &d);
  
  switch (d) {
  case SUNDAY:
    printf( "This is Sunday \n ");
    break;
  case MONDAY:
    printf( "This is Monday \n ");
    break;
  case TUESDAY:
    printf( "This is Tuesday \n ");
    break;
  case WEDNESDAY:
    printf( "This is Wednesday \n ");
    break;
  case THURSDAY:
    printf( "This is Thursday \n ");
    break;
  case FRIDAY:
    printf( "This is Friday \n ");
    break;
  case SATURDAY:
    printf( "This is Saturday \n ");
    break;
  default:
    printf("Invalid day \n");
    break; 
  }
} 

