#include <iostream>

enum Operation {PLUS, MINUS, DIVIDE, MULTIPLY}; 
enum Status {ERROR};

// Returns x <op> y.
template <class T>
T calculate(T x, Operation op, T y);


///
/// Implementation details.
///

template <class T>
T calculate(T x, Operation op, T y) {
    switch(op) {
        case Operation::PLUS:
            return x + y;
        case Operation::MINUS:
            return x - y;
        case Operation::DIVIDE:
            return x / y;
        case Operation::MULTIPLY:
            return x * y;
        default:
            throw Status::ERROR;
    }
}
