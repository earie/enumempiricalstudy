#include "calculator_constants.h"

#include <string>

int main(void) {
    float x = 5.0;
    float y = 2.5;

    try {
        float result = calculate<float>(x, 5, y);
        std::cout << result << std::endl;
    } catch (...) {
        std::cout << "Unhandled operator" << std::endl;
    }
}

