#include <iostream>

const int PLUS=1, MINUS=2, DIVIDE=3, MULTIPLY=4; 
const int ERROR = 5;

// Returns x <op> y.
template <class T>
T calculate(T x, int op, T y);


///
/// Implementation details.
///

template <class T>
T calculate(T x, int op, T y) {
    switch(op) {
        case PLUS:
            return x + y;
        case MINUS:
            return x - y;
        case DIVIDE:
            return x / y;
        case MULTIPLY:
            return x * y;
        default:
            throw ERROR;
    }
}

