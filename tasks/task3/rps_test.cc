#include "rps.h"

#include <gtest/gtest.h>

TEST(RockPaperScissorTest, DrawTest) {
    Move human_move = Move::ROCK;
    Move computer_move = Move::ROCK;

    Result result = result_for_human(human_move, computer_move);
    ASSERT_EQ(result, Result::DRAW);
}

int main (int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
