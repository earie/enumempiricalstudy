
#include <cstdlib>
#include <ctime>
#include <iostream>

enum Player { HUMAN, COMPUTER };

enum Move { ROCK, PAPER, SCISSOR };

enum Result { DRAW, WIN, LOSE };

// Given human_move and computer_move, this method returns
// whether the human player WON, LOST or the game was DRAW.
Result result_for_human(Move human_move, Move computer_move);

// Given the move, this method prints the move as a string.
// Eg. Move::ROCK is printed as "ROCK"
void print_move(Move move);

// Given human_result, this method prints the outcome of the
// game. 
// Eg. If it is a Result::DRAW, it prints out "It's a draw!".
void print_result(Result human_result);
