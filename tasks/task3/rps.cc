#include "rps.h"

Result result_for_human(Move human_move, Move computer_move) {
  if (human_move == computer_move) {
    return Result::DRAW;
  } else if (human_move == Move::ROCK) {
    if (computer_move == Move::SCISSOR) {
      return Result::WIN;
    } else {
      return Result::LOSE;
    }
  } else if (human_move == Move::SCISSOR) {
    if (computer_move == Move::ROCK) {
      return Result::LOSE;
    } else {
      return Result::WIN;
    }
  } else {
    if (computer_move == Move::ROCK) {
      return Result::WIN;
    } else {
      return Result::LOSE;
    }
  }
}

void print_move(Move move) {
  switch (move) {
  case Move::ROCK:
    std::cout << "ROCK" << std::endl;
    break;
  case Move::PAPER:
    std::cout << "PAPER" << std::endl;
    break;
  case Move::SCISSOR:
    std::cout << "SCISSOR" << std::endl;
    break;
  default:
    break;
  }
}

void print_result(Result human_result) {
  switch (human_result) {
  case Result::WIN:
    std::cout << "Human player won" << std::endl;
    break;
  case Result::LOSE:
    std::cout << "Computer player won" << std::endl;
    break;
  case Result::DRAW:
    std::cout << "It's a draw" << std::endl;
    break;
  default:
    break;
  }
}

//int main(void) {
//  std::cout << "Human: Enter your move ";
//  Move human_move, computer_move;
//
//  int temp;
//  std::cin >> temp;
//  player_move = static_cast<Move>(temp);
//  print_move(player_move);
//
//  std::srand(std::time(0));
//  std::cout << "Computer: Thinking ... ";
//  computer_move = static_cast<Move>(std::rand() % 3);
//  print_move(computer_move);
//
//  Result winner = result_for_human(human_move, computer_move);
//  print_result(winner);
//}
