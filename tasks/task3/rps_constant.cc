#include "rps_constant.h"

#include <cstdlib>
#include <iostream>
#include <ctime>

int result_for_human(int human_move, int computer_move) {
  if (human_move == computer_move) {
    return DRAW;
  } else if (human_move == ROCK) {
    if (computer_move == SCISSOR) {
      return WIN;
    } else {
      return LOSE;
    }
  } else if (human_move == SCISSOR) {
    if (computer_move == ROCK) {
      return LOSE;
    } else {
      return WIN;
    }
  } else {
    if (computer_move == ROCK) {
      return WIN;
    } else {
      return LOSE;
    }
  }
}

void print_move(int move) {
  switch (move) {
  case ROCK:
    std::cout << "ROCK" << std::endl;
    break;
  case PAPER:
    std::cout << "PAPER" << std::endl;
    break;
  case SCISSOR:
    std::cout << "SCISSOR" << std::endl;
    break;
  default:
    std::cout << "Here" << std::endl;
  }
}

void print_result(int human_result) {
  switch (human_result) {
  case WIN:
    std::cout << "Human player won" << std::endl;
    break;
  case LOSE:
    std::cout << "Computer player won" << std::endl;
    break;
  case DRAW:
    std::cout << "Its a draw" << std::endl;
    break;
  default:
    break;
  }
}

int main(void) {
  std::cout << "Human: Enter your move " << std::endl;
  int human_move, computer_move;
  
  std::cin >> human_move;
  print_move(human_move);

  std::srand(std::time(0));
  std::cout << "Computer: Thinking ... ";
  computer_move = std::rand() % 3;
  print_move(computer_move);

  int human_result = result_for_human(human_move, computer_move);
  print_result(human_result);
}
