
#include <cstdlib>
#include <ctime>
#include <iostream>

const int ROCK = 0;
const int PAPER = 1;
const int SCISSOR = 2;

const int DRAW = 3;
const int LOSE = 4;
const int WIN = 5;

const int HUMAN = 6;
const int COMPUTER = 7;

// Given human_move and computer_move, this method returns
// whether the human player WON, LOST or the game was DRAW.
int result_for_human(int human_move, int computer_move);

// Given the move, this method prints the move as a string.
// Eg. ROCK is printed as "ROCK"
void print_move(int move);

// Given human_result, this method prints the outcome of the
// game. 
// Eg. If it is a DRAW, it prints out "It's a draw!".
void print_result(int human_result);

