#include <iostream>

const int MERCURY = 1;
const int VENUS = 2;
const int EARTH = 3;
const int MARS = 4;
const int JUPITER = 5;
const int SATURN =6;
const int URANUS = 7;
const int NEPTUNE = 8;

// Given two planets 'first' and 'second', this method should
// return the planet that is closest to the Sun.
int closer_to_sun(const int first, const int second);

void print_planet_name(const int planet);


