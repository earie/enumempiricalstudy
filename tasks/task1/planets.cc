#include "planets.h"

Planet closer_to_sun(const Planet first, const Planet second) {
    Planet closer = first < second ? first : second;
    return closer;
}

void print_planet_name(const Planet planet) {
    switch(planet) {
        case Planet::MERCURY:
            std::cout << "MERCURY" << std::endl;
            break;
        case Planet::VENUS:
            std::cout << "VENUS" << std::endl;
            break;
        case Planet::EARTH:
            std::cout << "EARTH" << std::endl;
            break;
        case Planet::MARS:
            std::cout << "MARS" << std::endl;
            break;
        case Planet::JUPITER:
            std::cout << "JUPITER" << std::endl;
            break;
        case Planet::SATURN:
            std::cout << "SATURN" << std::endl;
            break;
        case Planet::URANUS:
            std::cout << "URANUS" << std::endl;
            break;
        case Planet::NEPTUNE:
            std::cout << "NEPTUNE" << std::endl;
            break;
        default:
            std::cout << "Not a planet" << std::endl;
    } 
}

int main(void) {
    Planet first = Planet::MERCURY;
    Planet second = Planet::EARTH;

    Planet closer = closer_to_sun(first, second);
    print_planet_name(first);
    print_planet_name(second);

    std::cout << "Planet close to sun is ";
    print_planet_name(closer);
}
