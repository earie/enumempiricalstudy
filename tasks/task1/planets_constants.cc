#include "planets_constants.h"

int closer_to_sun(const int first, const int second) {
    int closer = first < second ? first : second;
    return closer;
}

void print_planet_name(const int planet) {
    switch(planet) {
        case MERCURY:
            std::cout << "MERCURY" << std::endl;
            break;
        case VENUS:
            std::cout << "VENUS" << std::endl;
            break;
        case EARTH:
            std::cout << "EARTH" << std::endl;
            break;
        case MARS:
            std::cout << "MARS" << std::endl;
            break;
        case JUPITER:
            std::cout << "JUPITER" << std::endl;
            break;
        case SATURN:
            std::cout << "SATURN" << std::endl;
            break;
        case URANUS:
            std::cout << "URANUS" << std::endl;
            break;
        case NEPTUNE:
            std::cout << "NEPTUNE" << std::endl;
            break;
        default:
            std::cout << "Not a planet" << std::endl;
    } 
}

int main(void) {
    int first = MERCURY;
    int second = EARTH;

    int closer = closer_to_sun(first, second);
    print_planet_name(first);
    print_planet_name(second);

    std::cout << "Planet close to sun is ";
    print_planet_name(closer);
}

