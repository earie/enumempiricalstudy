#include <iostream>

enum Planet {MERCURY, VENUS, EARTH, MARS, JUPITER, SATURN, URANUS, NEPTUNE};

// Given two planets 'first' and 'second', this method should
// return the planet that is closest to the Sun.
Planet closer_to_sun(const Planet first, const Planet second);

void print_planet_name(const Planet planet);
