import java.util.*;

 public class EnumTest {

  public enum Day {
  SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY }

  Day day;

  public EnumTest(Day day) {
    this.day = day;
  }
  public void findDay() {
    switch (day) {
      case SUNDAY:
        System.out.println("This is Sunday");
        break;
      case MONDAY:
        System.out.println("This is Monday");
        break;
      case TUESDAY:
        System.out.println("This is Tuesday");
        break;
      case WEDNESDAY:
        System.out.println("This is Wednesday");
        break;
      case THURSDAY:
        System.out.println("This is Thursday");
        break;
      case FRIDAY:
        System.out.println("This is Friday");
        break;
      case SATURDAY:
        System.out.println("This is Saturday");
        break;
      default:
        System.out.println("Invalid day");
        break;
    }
  }
  
  public static void main(String[] args) {
   // int d;
   // Scanner d = d.nextInt();
   // System.out.println("Enter day");
    EnumTest firstDay = new EnumTest(Day.SUNDAY);
    firstDay.findDay();
    EnumTest secondDay = new EnumTest(Day.MONDAY);
    secondDay.findDay();
    EnumTest thirdDay = new EnumTest(Day.TUESDAY);
    thirdDay.findDay();
    EnumTest fourthDay = new EnumTest(Day.WEDNESDAY);
    fourthDay.findDay();
    EnumTest fifthDay = new EnumTest(Day.THURSDAY);
    fifthDay.findDay();
    EnumTest sixthDay = new EnumTest(Day.FRIDAY);
    sixthDay.findDay();
    EnumTest seventhDay = new EnumTest(Day.SATURDAY);
    seventhDay.findDay();
  }
} 



      


