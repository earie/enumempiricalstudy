#include <iostream>

using namespace std;

// enum DAYS {SUNDAY = 1, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY,
// SATURDAY};

int main(void) {
  cout << "Enter day" << endl;

  const int SUNDAY = 1;
  const int MONDAY = 2;
  const int TUESDAY = 3;
  const int WEDNESDAY = 4;
  const int THURSDAY = 5;
  const int FRIDAY = 6;
  const int SATURDAY = 7;

  int d;
  cin >> d;

  switch (d) {
  case SUNDAY:
    cout << "This is Sunday" << endl;
    break;
  case MONDAY:
    cout << "This is Monday" << endl;
    break;
  case TUESDAY:
    cout << "This is Tuesday" << endl;
    break;
  case WEDNESDAY:
    cout << "This is WEDNESDAY" << endl;
    break;
  case THURSDAY:
    cout << "This is THURSDAY" << endl;
    break;
  case FRIDAY:
    cout << "This is FRIDAY" << endl;
    break;
  case SATURDAY:
    cout << "This is SATURDAY" << endl;
    break;
  default:
    cout << "Invalid day" << endl;
  }
}
